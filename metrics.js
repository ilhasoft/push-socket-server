const client = require('prom-client');

const register = new client.Registry();

register.setDefaultLabels({
  app: 'push-socket-server',
});

client.collectDefaultMetrics({ register });

const socketRegistrations = new client.Histogram({
  name: 'socket_registrations',
  help: 'Registrations count per channel, hostApi and origin',
  labelNames: ['channel', 'hostApi', 'origin'],
});

const openConnections = new client.Gauge({
  name: 'open_connections',
  help: 'Open Connections count per channel, hostApi and origin',
  labelNames: ['channel', 'hostApi', 'origin'],
});

const clientMessages = new client.Histogram({
  name: 'client_messages',
  help: 'Histogram of client messages labeled by channel, hostApi, origin and status',
  labelNames: ['channel', 'hostApi', 'origin', 'status'],
});

register.registerMetric(socketRegistrations);
register.registerMetric(clientMessages);
register.registerMetric(openConnections);

module.exports = {
  register,
  socketRegistrations,
  openConnections,
  clientMessages,
};
