var SCWorker = require('socketcluster/scworker');
var fs = require('fs');
var express = require('express');
var serveStatic = require('serve-static');
var path = require('path');
var morgan = require('morgan');
var healthChecker = require('sc-framework-health-check');

// dependencies
const bodyParser = require('body-parser');
const request = require('request');
const country = require('country-language');
const url = require('url');
const os = require('os');
const axios = require('axios');
const https = require('https');
const qs = require('qs');

const { register, socketRegistrations, openConnections, clientMessages } = require('./metrics.js');

const agent = new https.Agent({
  rejectUnauthorized: false,
});

const suggestionsSecret = process.env.SUGGESTIONS_SECRET;

class Worker extends SCWorker {
  run() {
    console.log('   >> Worker PID:', process.pid);
    var environment = this.options.environment;

    var app = express();

    var httpServer = this.httpServer;
    var scServer = this.scServer;

    if (environment == 'dev') {
      // Log every HTTP request. See https://github.com/expressjs/morgan for other
      // available formats.
      app.use(morgan('dev'));
    }

    // Add GET /health-check express route
    healthChecker.attach(this, app);
    httpServer.on('request', app);

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    app.enable('trust proxy');

    app.get('/metrics', (req, res) => {
      // Return all metrics the Prometheus exposition format
      res.setHeader('Content-Type', register.contentType);
      res.end(register.metrics());
    });

    app.get('/ping', (req, res) => {
      res
        .status(200)
        .send(
          `pid: ${
            process.pid
          }<br> hostname: ${os.hostname()}<br> uptime: ${os.uptime()}<br> freemem: ${os.freemem()}`
        );
    });

    app.post('/', async (req, res) => {
      var quickReplies = req.body['quick_replies'];

      if (Array.isArray(quickReplies) && quickReplies.length > 0) {
        quickReplies = quickReplies.map((reply) => {
          return { title: reply };
        });
        req.body['metadata'] = { quick_replies: quickReplies };
      }
      scServer.exchange.publish(req.body.to, req.body);
      return res.status(200).send('success');
    });

    /*
      In here we handle our incoming realtime connections and listen for events.
    */
    scServer.on('connection', function (socket) {
      const data = url.parse(socket.request.url, true);

      if (data.query['channelUUID'] && data.query['hostApi']) {
        const commonMetricsLabels = {
          channel: data.query['channelUUID'],
          hostApi: data.query['hostApi'],
          origin: socket.request.headers.origin,
        };

        openConnections.inc(commonMetricsLabels);

        socket.on('registerUser', async (args, callBack) => {
          const end = socketRegistrations.startTimer();

          try {
            var user = { urn: args && args.id ? args.id : socket.id };
            callBack(JSON.stringify(user));
            subscribeToChannel(socket, user.urn);
          } catch (error) {
            console.log('register user: ', error);
          }

          end(commonMetricsLabels);
        });

        socket.on('sendMessageToChannel', async (args) => {
          const end = clientMessages.startTimer();

          try {
            const res = await sendMessageToChannel({
              channelUUID: data.query['channelUUID'],
              hostApi: data.query['hostApi'],
              ...args,
            });

            end({
              ...commonMetricsLabels,
              status: res.status,
            });
          } catch (err) {
            // if an error not realted to a failed request occurs
            end({
              ...commonMetricsLabels,
              status: 'internal-error',
            });
          }
        });

        socket.on('getSuggestions', async (args, callBack) => {
          if (args.repositories && args.language) {
            const options = {
              url: args.suggestionsUrl,
              headers: {
                'Content-Type': 'application/json',
                Authorization: suggestionsSecret,
              },
              data: {
                repositories: args.repositories,
                language: args.language,
                text: args.text,
                exclude_intents: args.excluded,
              },
            };

            axios
              .post(options.url, options.data, { headers: options.headers })
              .then((data) => {
                callBack(JSON.stringify(data.data));
              })
              .catch((e) => {
                console.log('error fetching suggestions', e);
              });
          }
        });
      }
    });

    scServer.on('disconnection', (socket) => {
      const data = url.parse(socket.request.url, true);

      if (data.query['channelUUID'] && data.query['hostApi']) {
        openConnections.dec({
          channel: data.query['channelUUID'],
          hostApi: data.query['hostApi'],
          origin: socket.request.headers.origin,
        });
      }
    });

    const subscribeToChannel = (socket, channelName) => {
      scServer.exchange.subscribe(channelName).watch((_responsePush) => {
        socket.emit('receivedMessageFromChannel', _responsePush);
      });
    };

    async function sendMessageToChannel(args) {
      if (args) {
        let response = null;
        const { channelUUID, hostApi, userUrn, text } = args;
        try {
          const params = qs.stringify({
            from: userUrn,
            text: text,
          });

          const options = {
            url: `${hostApi}/c/ex/${channelUUID}/receive`,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
          };

          const _response = await axios.post(options.url, params, {
            headers: options.headers,
            httpsAgent: agent,
          });
          response = _response.body;
        } catch (e) {
          if (e.response) {
            return e.response;
          } else {
            throw new Error(e);
          }
        }

        return response;
      }
    }
  }
}

new Worker();
