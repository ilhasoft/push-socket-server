FROM node:8-slim

LABEL maintainer="Elton Pereira de Lima"
LABEL maintainer_email="eltonplima AT ilhasoft DOT com DOT br"

RUN mkdir -p /usr/src/
WORKDIR /usr/src/
COPY . /usr/src/

RUN yarn install --ignore-engines

ENV SOCKETCLUSTER_WORKERS=8 SOCKETCLUSTER_PORT=80
EXPOSE 80

CMD ["yarn", "run", "start:docker"]
